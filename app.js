const colors = ["green", "red", "rgba(133,122,200)", "#f15025"];
const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];

const btn = document.getElementById("btn");
const color = document.querySelector(".color");

const linkSimples = document.getElementById("simples");
const linkHex = document.getElementById("hex");
let linkHexAtivo = false;

linkSimples.addEventListener("click", function () {
  setClassActive(linkSimples, true);
  setClassActive(linkHex, false);
  linkHexAtivo = false;
});

linkHex.addEventListener("click", function () {
  setClassActive(linkSimples, false);
  setClassActive(linkHex, true);
  linkHexAtivo = true;
});

btn.addEventListener("click", function () {
  const randomNumber = getRandomNumber();
  if (linkHexAtivo === true) {
    let hexColor = "#";
    for (let i = 0; i < 6; i++) {
      hexColor += hex[getRandomNumber()];
    }
    //console.log(hexColor);

    color.textContent = hexColor;
    document.body.style.backgroundColor = hexColor;
  } else {
    document.body.style.backgroundColor = colors[randomNumber];
    color.textContent = colors[randomNumber];
    //console.log(randomNumber);
  }
});

function getRandomNumber() {
  return Math.floor(Math.random() * colors.length);
}

function setClassActive(element, ok) {
  if (ok === true) {
    //element.classList.add("active");
    name = "active";
    arr = element.className.split(" ");
    if (arr.indexOf(name) == -1) {
      element.className += " " + name;
    }
  } else {
    //element.classList.remove("active");
    element.className = element.className.replace(/\bactive\b/g, "");
  }
}
